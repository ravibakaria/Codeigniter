<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
        {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->model('My_Model');
        }

	public function index()
	{	
		if($this->session->userdata('Admin_user')){
			redirect(base_url('Admin/dashbord'));
		}else{
			$this->load->view('admin/admin_login.php');
		}
	}

	public function login(){
		if($this->session->userdata('Admin_user')){
			redirect(base_url('Admin/dashbord'));
		}else{
			$this->form_validation->set_rules('email', 'Email', 'required');
			$this->form_validation->set_rules('pwd', 'Password', 'required');
			

	        if ($this->form_validation->run() == FALSE)
	        {
	            $this->load->view('admin/admin_login.php');
	        }
	        else
	        {	
	            $email = $this->input->post('email');
	            $pwd = $this->input->post('pwd');
	        	$data = $this->My_Model->admin_login($email,$pwd);
				$newdata = array(
			        'email'  => $email,
			        'logged_in' => TRUE
				);
				if(!empty($data)){
					$this->session->set_userdata('Admin_user',$data);
					redirect(base_url('Admin/dashbord'));
				}else{
					 $this->session->set_flashdata('message', 'Email or Password invalid');
					 redirect(base_url('Admin/login'));
				}
			}
				
	       }
	}
	public function logout(){
		$this->session->unset_userdata('Admin_user');
		redirect(base_url('Admin/login'));
	}
	public function dashbord(){
		if($this->session->userdata('Admin_user')){
			$data = $this->My_Model->product();
			$this->load->view('admin/dashbord.php');
		}else{
			redirect(base_url('Admin/login'));
		}

	}
	public function add_product(){
		if(!empty($this->session->userdata('Admin_user'))){
			$this->load->view('admin/addproduct.php');
		}else{
			redirect(base_url('Admin/login'));
		}
		
	}
	public function add_product_action(){
		if($this->session->userdata('Admin_user')){
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('pries', 'Pries', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$count = count($_FILES['fname']['size']);
				for($s=0; $s<=$count-1; $s++) {
					if (empty($_FILES['fname']['name'][$s]))
					{
					    $this->form_validation->set_rules('fname', 'Image', 'required');
					}
			}
	        if ($this->form_validation->run() == FALSE)
	        {
	            $this->load->view('admin/addproduct.php');

	        }
	        else
	        {	
	        	$userData = array(
	                'name' => $this->input->post('name'),
	                'pries' => $this->input->post('pries'),
	                'description' => $this->input->post('description')
            		);
            	$insertUserData = $this->My_Model->insert($userData);
            	$last_id = $this->db->insert_id();
	        	if(!empty($_FILES['fname']['name'])){
	                
					$count = count($_FILES['fname']['size']);
				        foreach($_FILES as $key=>$value){
					        for($s=0; $s<=$count-1; $s++) {
						        $_FILES['fname']['name']     = $value['name'][$s];
						        $_FILES['fname']['type']     = $value['type'][$s];
						        $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
						        $_FILES['fname']['error']    = $value['error'][$s];
						        $_FILES['fname']['size']     = $value['size'][$s]; 
						        $image_path = realpath(APPPATH . '../uploads');
				                $config['upload_path'] = $image_path;  
						        $config['allowed_types']        = 'gif|jpg|png|jpeg';
				                $config['max_size']             = 4800;
				                // $config['max_width']            = 1024;
				                // $config['max_height']           = 768;
						        $this->load->library('upload', $config);
						        if($this->upload->do_upload('fname')){
				                    // Uploaded file data
				                    $fileData = $this->upload->data();
				                    $name = $fileData['file_name'];
				                  
				                    $upload_data = $this->upload->data();
						            $data['filename'] = $upload_data['file_name'];
						            //print_r($data['filename']);exit();
						            $this->resize_image($data['filename']);
						           
				                    $userData = array(
							                'img_name' => $name,
							                'product_id' =>$last_id
						            		);
				                    $this->db->insert('images',$userData);
			                	} else{
			                		 $error = array('error' => $this->upload->display_errors());
			                		print_r($error);
			                	}
						       
					        }
					    }
				        //$picture= implode(',', $name_array);
            	}else{
               	 $picture = '';
            	}
            
            	$this->session->set_flashdata('message', 'product added successfully');
            	redirect(base_url('Admin/add_product'));
            
        	}
        }	

	}

	function resize_image($filename)
    {	
    	$image_path = realpath(APPPATH . '../uploads');
        $img_source = $image_path .'/'. $filename;
        $img_target = $image_path .'/thumbnails/'.$filename;
        echo $img_source."<br>";
        echo $img_target."<br>";
        // image lib settings
        $this->load->library('image_lib');
        $config['image_library']    = "gd2";      
	    $config['source_image']     = $img_source;
	    $config['new_image']        = $img_target;
	    //$config['create_thumb']     = TRUE;      
	    $config['maintain_ratio']   = TRUE;      
	    $config['width'] = "128";      
	    $config['height'] = "128";
      
        $this->image_lib->initialize($config);
        if(!$this->image_lib->resize()){
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }




	public function list_product(){
		if($this->session->userdata('Admin_user')){
			$data['products'] = $this->My_Model->product();
			$this->load->view('admin/listproduct.php',$data);
		}else{
			redirect(base_url('Admin/login'));
		}
	}
	public function edit_product($id=''){
		if($this->session->userdata('Admin_user')){
		$Id = base64_decode($id);
		$product['product'] = $this->db->get_where('product', array('id' => $Id))->row();
		if(empty($product['product'])){
				echo "404";
		}else{
			$this->load->view('admin/product_edit',$product);
		}
		
		}else{
			redirect(base_url('Admin/login'));
		}

	}
	public function delete_product($id){
		if($this->session->userdata('Admin_user')){
			$Id = base64_decode($id);
			$this->db->where('id', $Id);
	        $this->db->delete('product');
	        $this->db->where('product_id',$Id);
	        $this->db->delete('images');
	        redirect(base_url('Admin/list_product'));
    	}
	}
	public function delete_product_img($id){
		if($this->session->userdimage_deleteata('Admin_user')){
			$Id = base64_decode($id);
			$userData = array('image' => ' ');
			$insertUserData = $this->My_Model->update_product($Id,$userData);
	        redirect(base_url('Admin/edit_product/'.$id));
    	}
	}

	public function update_product($id){
		if($this->session->userdata('Admin_user')){
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('pries', 'Pries', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			// $count = count($_FILES['fname']['size']);
			// 	for($s=0; $s<=$count-1; $s++) {
			// 		if (empty($_FILES['fname']['name'][$s]))
			// 		{
			// 		    $this->form_validation->set_rules('fname', 'Image', 'required');
			// 		}
			// }
	        if ($this->form_validation->run() == FALSE){
	            
	        	 $product['product'] = $this->db->get_where('product', array('id' => $id))->row();
				$this->load->view('admin/product_edit',$product);
	        }
	        else{
	        	$userData = array(
                		'name' => $this->input->post('name'),
                		'pries' => $this->input->post('pries'),
                		'description' => $this->input->post('description'),
            			);
            			 $insertUserData = $this->My_Model->update_product($id,$userData);

	        	if(!empty($_FILES['fname']['name'])){
					$count = count($_FILES['fname']['size']);
				        foreach($_FILES as $key=>$value){
					        for($s=0; $s<=$count-1; $s++) {
						        $_FILES['fname']['name']     = $value['name'][$s];
						        $_FILES['fname']['type']     = $value['type'][$s];
						        $_FILES['fname']['tmp_name'] = $value['tmp_name'][$s];
						        $_FILES['fname']['error']    = $value['error'][$s];
						        $_FILES['fname']['size']     = $value['size'][$s]; 
						        $image_path = realpath(APPPATH . '../uploads');
				                $config['upload_path'] = $image_path;  
						        $config['allowed_types']        = 'gif|jpg|png|jpeg';
				                $config['max_size']             = 4800;
				                // $config['max_width']            = 1024;
				                // $config['max_height']           = 768;
						        $this->load->library('upload', $config);
						        if($this->upload->do_upload('fname')){
				                    // Uploaded file data
				                    $fileData = $this->upload->data();
				                    $name = $fileData['file_name'];
				                  
				                    $upload_data = $this->upload->data();
						            $data['filename'] = $upload_data['file_name'];
						            //print_r($data['filename']);exit();
						            $this->resize_image($data['filename']);
						           
				                    $userData = array(
							                'img_name' => $name,
							                'product_id' =>$id
						            		);
				                    $this->db->insert('images',$userData);
			                	} else{
			                		 $error = array('error' => $this->upload->display_errors());
			                		print_r($error);
			                	}
						       
					        }
					    }
					    $this->session->set_flashdata('message', 'product Update successfully');
            			redirect(base_url('Admin/list_product'));
            		
	            }
	             $this->session->set_flashdata('message', 'product Update successfully');
            			redirect(base_url('Admin/list_product'));
	           
	            
	        }     
	                
        }
	}
	public function order_list(){
		$this->load->view('admin/navbar');
		$sql = "SELECT orders.u_id, orders.invoice_id,orders.date, orders.total_amt, users.name,invoices.invoice_no FROM `orders` LEFT JOIN users ON users.id = orders.u_id LEFT JOIN invoices ON invoices.id = orders.invoice_id";
		$data['orders'] =  $this->db->query($sql)->result();
		//echo "<pre>";
		//print_r($data);
		$this->load->view('admin/products/orderlist',$data);
	}
	public function view_invoice($invoice){
		$this->load->view('admin/navbar');
		 $invoices['invoice'] = $this->db->get_where('orders_items', array('invoice_no' => $invoice))->result_array();
		//print_r($invoices['invoice']);exit;
		 if(empty($invoices['invoice'])){
		 	$this->session->set_flashdata('message_r', 'invoice not found');
		 	redirect(base_url('Admin/order_list'));
		 }else{
		 	 $this->load->view('admin/products/invoice',$invoices);
		 }
		 
	}
	public function getLists(){
        $data = $row = array();
       // print_r($_POST);exit();
        $memData = $this->My_Model->getRows($_POST);
        
        $i = $_POST['start'];
        foreach($memData as $member){
            $i++;
            $status = '<a href="'.base_url().'Admin/view_invoice/'.$member->invoice_no.'"'.' class="btn btn-success">View Invoice</a>';
            $created = date( 'jS M Y', strtotime($member->date));
            $data[] = array($i, $member->name, $member->invoice_no, $member->total_amt, $created,$status);
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->My_Model->countAll(),
            "recordsFiltered" => $this->My_Model->countFiltered($_POST),
            "data" => $data,
        );
        
        // Output to JSON format
        echo json_encode($output);
    }
    public function my_qr_code(){
    	$this->load->library('ciqrcode');
    	//echo "ravi";
    	$qr_image=rand().'.png';
    	//header("Content-Type: image/png");
    	$params['data'] = 'This is a text to encode become QR Code';
		$params['level'] = 'H';
		$params['size'] = 2;
		//$params['savename'] = base_url().'uploads/tes.png';
		$params['savename'] =FCPATH."uploads/assets/qr/".$qr_image;
		if($this->ciqrcode->generate($params))
		{
			//echo '';
			$data['img_url']=$qr_image; 
		}
		$this->load->view('admin/qrcode',$data);
		//$this->ciqrcode->generate($params);
		//print_r($params);
		
	}


	public function download_excel(){
		$sql = "SELECT  orders.invoice_id,orders.date, orders.total_amt, users.name,invoices.invoice_no FROM `orders` LEFT JOIN users ON users.id = orders.u_id LEFT JOIN invoices ON invoices.id = orders.invoice_id";
		$data =  $this->db->query($sql)->result();
		//echo "<pre>";
		//print_r($data);exit();
		$params2 = [];
		foreach ($data as $row) {
			$created = date( 'jS M Y', strtotime($row->date));
			$params['Coustmer Name'] = $row->name;
			$params['Invoice No'] = $row->invoice_no;
			$params['Total amt'] = $row->total_amt;
			$params['Order Date'] = $created;
			$params2 []= $params;
			
		}
		$this->excel_export($params2,'Orders');
	}
	

	public function excel_export($data,$filename){
	    $filename = $filename.".xls";
	    header("Content-Disposition: attachment; filename=\"$filename\"");
	    header("Content-Type: application/vnd.ms-excel");
	    // header("Content-Type: text/plain");
	    $isPrintHeader = false;
	    if (! empty($data)) {
	        foreach ($data as $row) {
	        	
	            if (! $isPrintHeader) {
	                echo implode("\t", array_keys($row)) . "\r\n";
	                $isPrintHeader = true;
	            }
	           echo implode("\t", array_values($row)) . "\r\n";
	        }
	    }
	    exit();
	}

	public function image_delete(){
		$id = $this->input->post('id');
		//echo "ravi";
		$this->db->delete('images', array('id' => $id));
	}


}
