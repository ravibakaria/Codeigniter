<?php
   
require APPPATH . 'libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: true");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
class Product extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */ 


	public function index_get($id = '')
	{
        $token = $this->input->get_request_header('token');
        if($token != ''){
            $this->load->library('token_authentication');
            $user_token =  $this->token_authentication->check_token($token);
            //$user = $this->db->get_where('users',['token'=>$token]);
            if($user_token == true){
                if(!empty($id)){
                $data = $this->db->get_where("product", ['id' => $id])->row_array();
                }else{
                    $data = $this->db->get("product")->result();
                }
             
                $this->response($data, REST_Controller::HTTP_OK);
            }
         }else{
            $this->response(['status'=>false,'message'=>"User not logged in plz log in first."], REST_Controller::HTTP_BAD_REQUEST);
         }
        
    	
      
    }

    public function product_get($id = '') 
    {
        if(!empty($id)){
            $data = $this->db->get_where("product", ['id' => $id])->row_array();
        }else{
            $data = $this->db->get("product")->result();
        }

        
            $this->response($data, REST_Controller::HTTP_OK);
      
    }

    public function register_post(){
        // echo "<pre>";
        // // get_request_header

        //   print_r($this->input->raw_input_stream);exit();
        // $email = $this->input->post('email');
        // $name = $this->input->post('name');
        // $password = $this->input->post('password');
        // echo $name .'<br>'.$email.'<br>'. $password;exit();
        $user = $this->input->raw_input_stream;
        $this->response([
                    'status' => TRUE,
                    'message' => 'User Register successful.',
                    'data' => $user
                ], REST_Controller::HTTP_OK);
        // if(!empty($email) && !empty($password)){
            
        //     // Check if any user exists with the given credentials
        //     $con['returnType'] = 'single';
        //     $con['conditions'] = array(
        //         'email' => $email,
        //         'password' => md5($password)
                
        //     );
        //     $user = $this->getRows($con);
        //     $token = $this->create_token($con);
        //     if(!$user){
        //         $data = array(
        //             'name'=>$name,
        //             'email'=>$email,
        //             'password'=>$password
        //         );

        //         $this->db->insert('Customer_Orders',$data);
        //       //  $this->db->insert()
        //         $this->db->set('token', $token); //value that used to update column  
        //         $this->db->where('id', $user['id']); //which row want to upgrade  
        //         $this->db->update('users');
        //         $user = $this->getRows($con);
        //         // Set the response and exit
        //         $this->response([
        //             'status' => TRUE,
        //             'message' => 'User login successful.',
        //             'data' => $user
        //         ], REST_Controller::HTTP_OK);
        //     }else{
        //         // Set the response and exit
        //         //BAD_REQUEST (400) being the HTTP response code
        //         $this->response([
        //             'status' => false,
        //             'message' => 'Wrong email or password.'
        //         ], REST_Controller::HTTP_BAD_REQUEST);
        //     }
        // }else{
        //     // Set the response and exit
        //     $this->response([
        //             'status' => false,
        //             'message' => 'Wrong email or password.'
        //         ], REST_Controller::HTTP_BAD_REQUEST);
        // }
    
    }

    public function login_get() {
        // Get the post data
        $email = $this->input->get('email');
        $password = $this->input->get('password');
        //echo $email."<br>".$password; exit();
        
        // Validate the post data
        if(!empty($email) && !empty($password)){
            
            // Check if any user exists with the given credentials
            $con['returnType'] = 'single';
            $con['conditions'] = array(
                'email' => $email,
                'password' => md5($password)
                
            );
            $user = $this->getRows($con);
            $token = $this->create_token($con);
            if($user){
                $this->db->set('token', $token); //value that used to update column  
                $this->db->where('id', $user['id']); //which row want to upgrade  
                $this->db->update('users');
                $user = $this->getRows($con);
                // Set the response and exit
                $this->response([
                    'status' => TRUE,
                    'message' => 'User login successful.',
                    'data' => $user
                ], REST_Controller::HTTP_OK);
            }else{
                // Set the response and exit
                //BAD_REQUEST (400) being the HTTP response code
                $this->response([
                    'status' => false,
                    'message' => 'Wrong email or password.'
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        }else{
            // Set the response and exit
            $this->response([
                    'status' => false,
                    'message' => 'Wrong email or password.'
                ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    function create_token ($customer_id) {
        $this->load->database();

        // ***** Generate Token *****
        $char = "bcdfghjkmnpqrstvzBCDFGHJKLMNPQRSTVWXZaeiouyAEIOUY!@#%";
        $token = '';
        for ($i = 0; $i < 47; $i++) $token .= $char[(rand() % strlen($char))];
        return $token;
    }
    
    function getRows($params = array()){
        $this->db->select('*');
        $this->db->from('users');
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach($params['conditions'] as $key => $value){
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("id",$params)){
            $this->db->where('id',$params['id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $this->db->count_all_results();    
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->row_array():false;
            }else{
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():false;
            }
        }

        //return fetched data
        return $result;
    }


    public function status_get(){
        $this->load->library('someclass');
        echo $this->someclass->hello();
    }


    public function update_put($id){
        $name = $this->input->get('name');
        $pries =  $this->input->get('pries');
        $Description = $this->input->get('Description');
        $data = ['name'=>$name,'pries'=>$pries,'Description'=>$Description];
        $this->db->where('id',$id); //set column_name and value in which row need to update
        $product = $this->db->update('product',$data);
        if($product == TRUE){
            $this->response([
                    'status' => TRUE,
                    'message' => 'product update successful.',
                ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                    'status' => TRUE,
                    'message' => 'User login successful.'
                    
                ], REST_Controller::HTTP_BAD_REQUEST);
        }
        
    }

    public function readImage_post(){
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
        print_r($_FILES['upfile']);
        // ini_set('max_execution_time', 300); 
        // $image_path = realpath(APPPATH . '../uploads');
        // $config['upload_path']          = $image_path; 
        //         $config['allowed_types']        = 'jpg|jpeg|jfif';
        //         $config['max_size']             = 4800;
        //         $config['max_width']            = 1024;
        //         $config['max_height']           = 768;
        //         $this->load->library('upload', $config);
        //         if ( ! $this->upload->do_upload('upfile'))
        //         {
        //             $error = array('error' => $this->upload->display_errors());
        //             $this->response([
        //                 'data' => $error,
        //                 'status' => TRUE,
        //                 'message' => 'Error.'
                        
        //             ], REST_Controller::HTTP_BAD_REQUEST);
        //            // print_r($error);
        //         }
        //         else
        //         {
        //             $upload_data = $this->upload->data();
        //             $data['filename'] = $upload_data['file_name'];
        //             //print_r($data['filename']);
        //             $file2 = realpath(APPPATH . '../uploads/'.$data['filename']);
    
                

        //             $ch = curl_init('http://bcr1.intsig.net/BCRService/BCR_VCF2?PIN=&user=ravibakaria4@gmail.com&pass=CYCRFL4YBY737CYY&json=1&lang=15&size=59109');
                            
        //                     $cfile = new CURLFile($file2,'image/jpeg','test_name');
                            
        //                     // Assign POST data
        //                     $data = array('test_file' => $cfile);
        //                     curl_setopt($ch, CURLOPT_POST,1);
        //                     curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        //                     curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                            
        //                     // Execute the handle
        //                 //  curl_exec($ch);
        //             $result = curl_exec($ch);
        //             $result1 = json_decode($result);
               
        //             if(!$result){die("Connection Failure");}
        //             curl_close($ch);
                    
        //             if($result){
        //                 $this->response([
        //                         'data'=> $result1,
        //                     ], REST_Controller::HTTP_OK);
        //             }else{
        //                 $this->response([
        //                         'status' => TRUE,
        //                         'message' => 'Error.'
                                
        //                     ], REST_Controller::HTTP_BAD_REQUEST);
        //             }
        //         }
        #guzzle library add to use guzzle
            // $this->load->library('guzzle');
            
            // # guzzle client define
            // $client     = new GuzzleHttp\Client();
            
            // #This url define speific Target for guzzle
            // //$url        = 'http://www.google.com';

            // #guzzle
            // try {
            //     # guzzle post request example with form parameter
            //     $response = $client->request( 'POST', 
            //                                 $url, 
            //                                 [ 'form_params' 
            //                                         => [ 'upfile' => $file1 ] 
            //                                 ]
            //                                 );
            //     #guzzle repose for future use
            //     echo $response->getStatusCode(); // 200
            //     echo $response->getReasonPhrase(); // OK
            //     echo $response->getProtocolVersion(); // 1.1
            //     echo $response->getBody();
            // } catch (GuzzleHttp\Exception\BadResponseException $e) {
            //     #guzzle repose for future use
            //     $response = $e->getResponse();
            //     $responseBodyAsString = $response->getBody()->getContents();
            //     print_r($responseBodyAsString);
            // }
    }

}