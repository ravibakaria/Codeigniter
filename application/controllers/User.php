<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	
	public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('My_Model');
        $this->load->library('cart');
        $this->load->library('google');
        $this->load->library('facebook');
    }

    
    public function index(){
    	$data['products'] = $this->My_Model->product();
    	$this->load->view('user/listproduct.php',$data);
    }
	public function demo(){
    	
    	$this->load->view('user/html.php');
    }


    public function login(){
        $data['login_url'] = $this->facebook->getLoginUrl(array('redirect_uri' => base_url('User/login')));
        $data['google_login_url']=$this->google->get_login_url();
        $this->load->view('user/login.php',$data);
    }

    public function oauth2callback(){
        $google_data=$this->google->validate();
        $session_data=array(
                'name'=>$google_data['name'],
                'email'=>$google_data['email'],
                'source'=>'google',
                'profile_pic'=>$google_data['profile_pic'],
                'link'=>$google_data['link'],
                'sess_logged_in'=>1
                );
        $user = $this->db->get_where('users',['email'=>$google_data['email']]);
        //print_r($user);exit();
        if($user->num_rows() ==''){
        $data = array('name' => $google_data['name'],
                        'email'=>$google_data['email'],
                        'password'=> md5('12345678')
                    );
            $this->db->insert('users',$data);
        }
        // echo "<pre>";
        // print_r($google_data);exit();
            //$this->session->set_userdata($session_data);
        $data = $this->My_Model->google_login($google_data['email']);
         $this->session->set_userdata('user',$data);
        redirect(base_url('User'));
    }

    public function logout(){
        $this->session->unset_userdata('user');
        // $this->facebook->destroy_session();
        redirect(base_url('User/login'));
    }


    public function login_action(){
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('pwd', 'Password', 'required');
        if ($this->form_validation->run() == FALSE){
            $this->load->view('user/login.php');
        }else{   
            $email = $this->input->post('email');
            $pwd = $this->input->post('pwd');
            $data = $this->My_Model->user_login($email,$pwd);
            if(!empty($data)){
                $newdata = array(
                'email'  => $email,
                'logged_in' => TRUE
                );
                $this->session->set_userdata('user',$data);
                redirect(base_url('User'));
            }else{
                $this->session->set_flashdata('message', 'Email or Password invalid');
                redirect(base_url('User/login'));
            }        
        }
    }


    public function product_detail($id){
        $product['images'] = $this->db->get_where('images', array('product_id' => $id))->result();
       // print_r($product['product']);exit();
        $this->load->view('user/productdetail.php',$product);
    }


    public function show_cart(){
        $this->load->view('user/cart_product.php');
    }


    public function add_cart(){
        $id = $this->input->post('id');
        $qty = $this->input->post('qty');
        $data = $this->db->get_where('product', array('id' => $id))->row();
        $data1 = array(
                'id'      => $data->id,
                'qty'     => $qty,
                'price'   => $data->pries,
                'name'    => $data->name,
                'image'   => $data->image,
                'Description'   => $data->Description
        );   
        $this->cart->insert($data1);
        redirect(base_url('User'));

    }


    public function update_cart(){
        $post = $this->input->post();        
        if($this->input->post('payment')){
             for ($i=1; $i <= count($post) ; $i++) { 
                if(isset($post[$i]['rowid'])){
                    $data2 ['rowid']=$post[$i]['rowid'];
                    unset($post[$i]['rowid']);
                    unset($post[$i]['id']);
                    $insert = $this->db->insert('orders_items', $post[$i]);
                }
            }
            $this->paytmpost($post);
        }else{
            if(isset($post['order'])){
                $data1 = $post;
                $this->cart->update($data1);
                if($this->session->userdata('user')){
                    $data2 = [];
                    $invoice = array('invoice_no'=>$post['invoice_no']);
                    $invoice_id = $this->My_Model->insert1('invoices', $invoice);
                    for ($i=1; $i <= count($post) ; $i++) { 
                        if(isset($post[$i]['rowid'])){
                            $data2 ['rowid']=$post[$i]['rowid'];
                            unset($post[$i]['rowid']);
                            unset($post[$i]['id']);
                            $insert = $this->db->insert('orders_items', $post[$i]);
                            $post[$i]['qty'] = '0';
                            $data2 ['qty']=$post[$i]['qty'];
                            $this->cart->update($data2);
                        }
                    }
                    $order = array('u_id'=>$this->session->userdata('user')[0]->id,
                            'invoice_id'=>$invoice_id,
                            'total_amt'=>$post['total_amt'],
                            'paymet_status' => 'pending'
                            );
                    $this->db->insert('orders', $order);
                    $qr_code = $post['invoice_no'];
                    $qr_image = $post['invoice_no'].'.png';
                    $this->load->library('ciqrcode');
                    $params['data'] = $qr_code;
                    $params['level'] = 'H';
                    $params['size'] = 3;
                    $params['savename'] =FCPATH."uploads/assets/qr/".$qr_image;
                    $this->ciqrcode->generate($params);
                    redirect(base_url('User'));
                }else{
                    redirect(base_url('User/login'));
                }
            }else{
                $data = $post;
                $this->cart->update($data);
                redirect(base_url('User/show_cart'));
            }
        }
    }


    public function sign_up(){
        $this->load->view('user/register.php');
    }


    public function sign_up_action(){
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[4]|max_length[12]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

        if ($this->form_validation->run() == FALSE){
                $this->load->view('user/register.php');
        }else{
            $data = array('name' => $this->input->post('name'),
                        'email'=>$this->input->post('email'),
                        'password'=> md5($this->input->post('password'))
                    );
            $this->db->insert('users',$data);
            $this->session->set_flashdata('message_re', 'Sign up successfully');
            redirect(base_url('User/login'));
        }
    }


    public function show_order(){
        $this->load->view('user/user_nav.php');
        $id = $this->session->userdata('user')[0]->id;
       
        $sql = "SELECT orders.u_id, orders.invoice_id,orders.date, orders.total_amt, users.name,invoices.invoice_no FROM `orders` LEFT JOIN users ON users.id = orders.u_id LEFT JOIN invoices ON invoices.id = orders.invoice_id WHERE orders.u_id = $id ";
        $data['orders'] =  $this->db->query($sql)->result();
        //echo "<pre>";
        //print_r($data);
        $this->load->view('user/user_orders',$data);
    }


    public function view_invoice($invoice){
        // $this->load->view('user/user_nav.php');
        //$this->load->library('pdf');
        $mpdf = new \Mpdf\Mpdf();
        $invoices['invoice'] = $this->db->get_where('orders_items', array('invoice_no' => $invoice))->result_array();
        //$this->load->view('admin/products/invoice',$invoices);
        $html = $this->load->view('user/invoice',$invoices,true);
        //$html = $this->load->view('user/test',[],true);
        $mpdf->WriteHTML($html);
        $mpdf->Output('test.pdf','D');
        // $this->pdf->load_view('admin/products/invoice',$invoices);
        // $this->pdf->render();
        // $this->pdf->stream("invoices.pdf");
    }


    function mypdf(){
        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('welcome_message',[],true);
        $mpdf->WriteHTML($html);
        $mpdf->Output();
   }


    function paytm(){
        $this->load->view('user/test');
    }


    function paytmpost($data=[]){
        header("Pragma: no-cache");
        header("Cache-Control: no-cache");
        header("Expires: 0");
        // following files need to be included
        require_once(APPPATH . "third_party/PaytmKit/lib/config_paytm.php");
        require_once(APPPATH . "third_party/PaytmKit/lib/encdec_paytm.php");

        $checkSum = "";
        $paramList = array();

        $ORDER_ID = $_POST["invoice_no"];
        $CUST_ID = $_POST["CUST_ID"];
        $INDUSTRY_TYPE_ID = $_POST["INDUSTRY_TYPE_ID"];
        $CHANNEL_ID = $_POST["CHANNEL_ID"];
        $TXN_AMOUNT = $_POST["total_amt"];
         //$MSISDN = $_POST["MOBILE_NO"];

        // Create an array having all required parameters for creating checksum.
        $paramList["MID"] = PAYTM_MERCHANT_MID;
        $paramList["ORDER_ID"] = $ORDER_ID;
        $paramList["CUST_ID"] = $CUST_ID;
        $paramList["INDUSTRY_TYPE_ID"] = $INDUSTRY_TYPE_ID;
        $paramList["CHANNEL_ID"] = $CHANNEL_ID;
        $paramList["TXN_AMOUNT"] = $TXN_AMOUNT;
        $paramList["WEBSITE"] = PAYTM_MERCHANT_WEBSITE;
        $paramList["CALLBACK_URL"] = "http://localhost:8080/CodeIgniter/User/paytm_response";
         //$paramList["MSISDN"] = $MSISDN;
         /*

         $paramList["MSISDN"] = $MSISDN; //Mobile number of customer
         $paramList["EMAIL"] = $EMAIL; //Email ID of customer
         $paramList["VERIFIED_BY"] = "EMAIL"; //
         $paramList["IS_USER_VERIFIED"] = "YES"; //

         */
         // echo "<pre>";
         // print_r($paramList);exit();

        //Here checksum string will return by getChecksumFromArray() function.
        $checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
         //echo $checkSum; 
         echo "<html>
        <head>
        <title>Merchant Check Out Page</title>
        </head>
        <body>
            <center><h1>Please do not refresh this page...</h1></center>
                <form method='post' action='".PAYTM_TXN_URL."' name='f1'>
        <table border='1'>
        <tbody>";
        foreach($paramList as $name => $value) {
            echo '<input type="hidden" name="' . $name .'" value="'.$value.'">';
        }
        echo "<input type='hidden' name='CHECKSUMHASH' value='".$checkSum."'> 
        </tbody>
        </table>
        <script type='text/javascript'>
            document.f1.submit();
        </script>
        </form>
        </body>
        </html>";
    } 


    public function paytm_response(){
        //echo $this->session->userdata('user')[0]->id;exit();
        $paytmChecksum  = "";
        $paramList      = array();
        $isValidChecksum= "FALSE";
        $paramList = $_POST;
        $paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg
        header("Pragma: no-cache");
        header("Cache-Control: no-cache");
        header("Expires: 0");
        require_once(APPPATH . "third_party/PaytmKit/lib/config_paytm.php");
        require_once(APPPATH . "third_party/PaytmKit/lib/encdec_paytm.php");
        //Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application’s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
        $isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.
        if($isValidChecksum == "TRUE") {
            if (isset($_POST) && count($_POST)>0 ){   
                $data = array(
                    'ORDERID'=>$_POST['ORDERID'],
                    'TXNID'=>$_POST['TXNID'],
                    'TXNAMOUNT'=>$_POST['TXNAMOUNT'],
                    'PAYMENTMODE'=>$_POST['PAYMENTMODE'],
                    'TXNDATE'=>$_POST['TXNDATE'],
                    'STATUS'=>$_POST['STATUS'],
                    'RESPMSG'=>$_POST['RESPMSG'],
                    'GATEWAYNAME'=>$_POST['GATEWAYNAME'],
                    'BANKTXNID'=>$_POST['BANKTXNID'],
                    'BANKNAME'=>$_POST['BANKNAME'],
                    'CHECKSUMHASH'=>$_POST['CHECKSUMHASH'],
                    'DATA'=> serialize($_POST)
                );
                $this->db->insert('payments', $data);
                if($_POST['RESPCODE'] == '01'){
                    $invoice = array('invoice_no'=>$_POST['ORDERID']);
                    $invoice_id = $this->My_Model->insert1('invoices', $invoice);
                    $u_id =  $this->session->userdata('user')[0]->id;
                    $qr_code = $_POST['ORDERID'];
                    $qr_image = $_POST['ORDERID'].'.png';
                    $this->load->library('ciqrcode');
                    $params['data'] = $qr_code;
                    $params['level'] = 'H';
                    $params['size'] = 3;
                    $params['savename'] =FCPATH."uploads/assets/qr/".$qr_image;
                    $this->ciqrcode->generate($params);
                    $order = array('u_id'=>$u_id,
                                'invoice_id'=>$invoice_id,
                                'total_amt'=>$_POST['TXNAMOUNT'],
                                'paymet_status' => 'Done'
                            );
                    $this->db->insert('orders', $order);
                    $this->cart->destroy();
                    $this->session->set_flashdata('message', $_POST['RESPMSG']);
                   redirect(base_url('User'));
                }else{
                    $this->db->delete('orders_items', array('invoice_no' => $_POST['ORDERID']));
                    $this->session->set_flashdata('message_re', $_POST['RESPMSG']);
                    redirect(base_url('User'));
                }
            }
        }else{
            echo "<b>Checksum mismatched.</b>";
            //Process transaction as suspicious.
        }
    }



    public function flogin()
    {

       // echo "hsdgjhdgdfhgkjdf";exit();
        $user = "";
        $userId = $this->facebook->getUser();
        if ($userId) {
            try {
                $user = $this->facebook->api('/me');
            } catch (FacebookApiException $e) {
                $user = "";
            }
        }
        else {
            echo 'Please try again.'; exit;
        }
        if($user!="") :
           echo '<pre>'; print_r($user); exit;  
           //Write here login script    
        else :
            $data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('welcome/flogin'), 
                'scope' => array("email") // permissions here
            ));
        endif;
        
    }
}