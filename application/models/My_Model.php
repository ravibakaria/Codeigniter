<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Model extends CI_Model {
	   function __construct() {
        // Set table name
        //$this->table = 'members';
        // Set orderable column fields
        $this->column_order = array(null, 'name','invoice_no','total_amt', 'date','status');
        // Set searchable column fields
       $this->column_search = array('name','invoice_no','date','total_amt');
        // Set default order
        $this->order = array('name' => 'asc');
    }

	public function admin_login($email,$pwd){
			$pass = md5($pwd);
			$query = $this->db->get('admin'); 
			
			$this->db->select("*");
		    $this->db->from("admin");
		    $this->db->where('email',$email);
		    $this->db->where('password ',$pass);
		    $q = $this->db->get();
		    //echo $this->db->last_query();
		    return $q->result();
	}
    public function google_login($email){
           
            $query = $this->db->get('users'); 
            
            $this->db->select("*");
            $this->db->from("users");
            $this->db->where('email',$email);
           
            $q = $this->db->get();
            //echo $this->db->last_query();
            return $q->result();
    }
	public function user_login($email,$pwd){
			$pass = md5($pwd);
			$query = $this->db->get('admin');
			
			$this->db->select("*");
		    $this->db->from("users");
		    $this->db->where('email',$email);
		    $this->db->where('password ',$pass);
		    $q = $this->db->get();
		    //echo $this->db->last_query();
		    return $q->result();
	}
	public function product(){
		// $this->db->select("*");
	 //    $this->db->from("product");
	   
	    $q = $this->db->get('product');
	    return $q->result();
		}

	public function insert($data = array()){
        
        $insert = $this->db->insert('product',$data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;    
        }
    }
    public function insert1($table, $data = array()){
        
        $insert = $this->db->insert($table,$data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;    
        }
    }
    public function update_product($id, $data) {
            $this->db->where('id',$id);
            return $this->db->update('product',$data);       
    }
     public function getRows($postData){
        $this->_get_datatables_query($postData);
        if($postData['length'] != -1){
            $this->db->limit($postData['length'], $postData['start']);
        }
        $query = $this->db->get();
       // echo $this->db->last_query();exit();
        return $query->result();
    }
    
   
    public function countAll(){
        //$this->db->from($this->table);
        $this->db->select('orders.u_id, orders.invoice_id,orders.date, orders.total_amt, users.name,invoices.invoice_no');
	    $this->db->from('orders');
	    $this->db->join('users', 'users.id = orders.u_id', 'left'); 
	    $this->db->join('invoices', 'invoices.id = orders.invoice_id', 'left'); 
        return $this->db->count_all_results();
    }
    
   
    public function countFiltered($postData){
        $this->_get_datatables_query($postData);
        $query = $this->db->get();
        return $query->num_rows();
    }
    private function _get_datatables_query($postData){
        $this->db->select('orders.u_id, orders.invoice_id,orders.date, orders.total_amt, users.name,invoices.invoice_no');
	    $this->db->from('orders');
	    $this->db->join('users', 'users.id = orders.u_id', 'left'); 
	    $this->db->join('invoices', 'invoices.id = orders.invoice_id', 'left'); 
	    //$query = $this->db->get();
	    //return $query->result();
       // $this->db->from('orders');

 
        $i = 0;
        // loop searchable columns 
        foreach($this->column_search as $item){
            // if datatable send POST for search
            if($postData['search']['value']){
                // first loop
                if($i===0){
                    // open bracket
                    $this->db->group_start();
                    $this->db->like($item, $postData['search']['value']);
                }else{
                    $this->db->or_like($item, $postData['search']['value']);
                }
                
                // last loop
                if(count($this->column_search) - 1 == $i){
                    // close bracket
                    $this->db->group_end();
                }
            }
            $i++;
        }
         
        if(isset($postData['order'])){
            $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


}
?>