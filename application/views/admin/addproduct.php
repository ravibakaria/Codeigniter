<?php include('navbar.php');?>
<div class="container">


  <div class="row">
  <?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>
  <div class ="col-md-4">
		  <h2>Add Product</h2>
  <div style = "text-align: right";>
	<a href="<?= base_url().'Admin/list_product';?>" class="btn btn-primary">List Of Product</a>
</div>
  	<form action="<?= base_url('Admin/add_product_action')?>" method = "POST" enctype="multipart/form-data">
    <div class="form-group">
      <label for="email">Name:</label>
      <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo set_value('name'); ?>">
       <?php echo form_error('name'); ?>
    </div>
     <div class="form-group">
      <label for="pries">Pries:</label>
      <input type="text" class="form-control" id="pries" placeholder="Enter pries" name="pries" value="<?php echo set_value('pries'); ?>">
      <?php echo form_error('pries'); ?>
    </div>
    <div class="form-group">
      <label for="pwd">Description:</label>
      <input type="text" class="form-control" id="description" placeholder="Enter description" name="description" value="<?php echo set_value('description'); ?>">
       <?php echo form_error('description'); ?>
    </div>
    <div class="form-group">
      <label for="pwd">Image:</label> 
      <input type="file" class="form-control" id="fname" name="fname[]"> 
      <div class="input_fields_wrap">
        
        </div>
      <button class="add_field_button">Add More</button>
       <?php echo form_error('fname'); ?>
    </div>
    
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
  </div>
  <div>
  	<!-- <img src="<?php echo base_url().'uploads/download.jpeg' ?>"> -->
  </div>
   </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
  var max_fields      = 5; //maximum input boxes allowed
  var wrapper       = $(".input_fields_wrap"); //Fields wrapper
  var add_button      = $(".add_field_button"); //Add button ID
  
  var x = 1; //initlal text box count
  $(add_button).click(function(e){ //on add input button click
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
      x++; //text box increment
      $(wrapper).append('<div><input type="file" class="form-control" id="fname" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
    }
  });
  
  $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent('div').remove(); x--;
  })
});
</script>