<?php include('navbar.php');?>
<form method="post" action="<?php echo base_url('Admin/update_product/'.$product->id);?>" enctype="multipart/form-data">
    <div class="row">
     
        <div class="col-md-4 col-md-offset-4">
        <h3>Update Product </h3>
            <div class="form-group">
      <label for="email">Name:</label>
      <input type="text" class="form-control" id="name" value="<?= $product->name;?>" name="name">
       <?php echo form_error('name'); ?>
    </div>
     <div class="form-group">
      <label for="pries">Pries:</label>
      <input type="text" class="form-control" id="pries" value="<?= $product->pries;?>" name="pries">
      <?php echo form_error('pries'); ?>
    </div>
    <div class="form-group">
      <label for="pwd">Description:</label>
      <input type="text" class="form-control" id="description" value="<?= $product->Description;?>" name="description">
       <?php echo form_error('description'); ?>
    </div>
     <?php $images = $this->db->get_where('images',['product_id'=>$product->id])->result();
     if(count($images)>=5){
      
     }else{
      
   
     ?>
     <div class="form-group">
      <label for="pwd">Image:</label> 
      <input type="file" class="form-control" id="fname" name="fname[]"> 
      <div class="input_fields_wrap">
        
        </div>
      <button class="add_field_button">Add More</button>
      
    </div>
    <?php } ?>
    <div class="row">
   
    <?php foreach ($images as $key => $image) {?>
      <div class="col-md-2">
       <button type="button" class="close" value="<?= $image->id?>">&times;</button>
        <img src="<?= base_url().'uploads/thumbnails/'.$image->img_name?>">
      </div>
      <?php } ?>
    </div>
       
   
 
    
    <button type="submit" class="btn btn-default">Save</button>
        </div>
    </div>
    
</form>
<script type="text/javascript">
$(document).ready(function(){
  $(".close").click(function(){
    if(confirm("Are you sure you want to delete this?")){
        
    }
    else{
        return false;
    }
    var id = $(this).val();
    //alert(id);
    $.ajax({ 
        url: "<?= base_url('Admin/image_delete')?>",
        data: {'id':id},
        type: 'post',
        success: function(data){
            //alert(data);
            location.reload();
          }
    });
  });
});

$(document).ready(function() {
  var max_fields      = <?php echo 5 - count($images);?>; //maximum input boxes allowed
  var wrapper       = $(".input_fields_wrap"); //Fields wrapper
  var add_button      = $(".add_field_button"); //Add button ID

  var x = 1; //initlal text box count
  $(add_button).click(function(e){ //on add input button click
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
      x++; //text box increment
      $(wrapper).append('<div><input type="file" class="form-control" id="fname" name="fname[]"> <a href="#" class="remove_field">   X </a></div>'); //add input box
    }
  });

  $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
    e.preventDefault(); $(this).parent('div').remove(); x--;
  })
});

</script>