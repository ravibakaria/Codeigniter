<?php include('navbar.php');?>
<div class="container">
<h2>List Of Products</h2>

<div style = "text-align: right";>
	<a href="<?= base_url().'Admin/add_product';?>" class="btn btn-primary">Add Product</a>
</div>
<?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>
<?php 
//print_r($products);?>
<table class="table table-striped">
    <thead>
      <tr>
        <th>Sr. No</th>
        <th>Name</th>
        <th>Pries</th>
        <th>Description</th>
        <th>Image</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php $i = 0;
    	foreach ($products as $key => $value) { $i++;?>
    	<tr>
    	<td><?= $i;?></td>
    	<td><?= $value->name;?></td>
    	<td><?= $value->pries;?></td>
    	<td><?= $value->Description;?></td>
    	<?php $img = $this->db->get_where('images',['product_id'=>$value->id])->row(); ?>
    	<td><img src="<?= base_url().'uploads/thumbnails/'.$img->img_name;?>" width="100" height="70"></td>
    	<td><a href="<?= base_url().'Admin/edit_product/'. base64_encode($value->id)?>" class="btn btn-primary">Edit</a>
    	<a href="<?= base_url().'Admin/delete_product/'. base64_encode($value->id)?>" class="btn btn-danger">Delete</a></td>    
    	</tr>    
    <?php
    	}
    ?>
    </tbody>
  </table>
</div>

</body>
</html>