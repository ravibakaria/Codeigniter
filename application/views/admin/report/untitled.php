<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>

 
<?php 


$begin = new DateTime( '2019-05-29' );
$end = new DateTime( '2019-06-04' );
$end = $end->modify( '+1 day' ); 

$interval = new DateInterval('P1D');
$daterange = new DatePeriod($begin, $interval ,$end);
 $data = [];
foreach($daterange as $date){
     $day = $date->format("Y-m-d");
  foreach ($payments as $key => $value) {
    $date = $value->orderDate;
    //if ( strtotime($day) == strtotime($date) ){
    if($day == $date){
      // echo $day.'<br>';
      // echo $value->amt.'<br>';
      $data [$day]= $value->amt;
   }  
  } 
 }
  //  echo "<pre>";
  // print_r($data);
?>
<div class="container">
  <div class="row">
    <div class ="col-md-4">
      <div class="form-group">
        <input type="text" class="form-control" id="datepicker" placeholder="Start Date" name="to" autocomplete="off">
      </div>
    </div>
    <div class ="col-md-4">
      <div class="form-group">
        <input type="text" class="form-control" id="datepicker1" placeholder="End Date" name="from" autocomplete="off">
      </div>
    </div>
    <div class ="col-md-4">
      <div class="form-group">
        <button type="button" class="btn btn-success getsale">GET SALE</button>
      </div>
    </div>
  </div>
  <div id="test" style="display: none;">
  
  </div>
  <div id="test1">
    <div class="row">
      <div class ="col-md-6" style="border: 1px solid black;">
        <div id="linechart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      </div>
      <div class ="col-md-6" style="border: 1px solid black;">
        <div id="Barchart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      </div>
    </div>
  
  <br>
  <div class="row">
     <div class="col-md-6 col-md-offset-3" style="border: 1px solid black;">
     <div id="piechart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
     </div>
  </div>
  </div>
</div>
<script type="text/javascript">
  Highcharts.chart('linechart', {
 chart: {
        type: 'line'
    },
  title: {
    text: 'Sale in linechart Last week'
  },
  yAxis: {
    title: {
      text: 'Amount of sales'
    }
  },
   xAxis: {
        categories: [<?php foreach ($data as $key => $value) {
          $days = date('d', strtotime($key));echo $days; echo ","; }?>]
    },
  plotOptions: {
    series: {
      label: {
        connectorAllowed: false
      },
    }
  },

  series: [{
    name: 'Sale',
    data: [<?php foreach ($data as $key => $value) {echo $value; echo ","; }?>]
  }],

  

});


</script>
<script type="text/javascript">
  $(function() {
    $( "#datepicker" ).datepicker({
      dateFormat: "dd-mm-yy",
      maxDate: 0 
    });
  } );
  $(function() {
    $( "#datepicker1" ).datepicker({
      dateFormat: "dd-mm-yy",
      maxDate: 0 
    });

  } );

$('.getsale').click(function() {
  var to = $('#datepicker').val();
  var from = $('#datepicker1').val();
  //alert(from); return false;
  if(to == ''){
    alert('Enter start date');return false;
  }
   if(from == ''){
    alert('Enter end date');return false;
  }
    $.ajax({ 
        url: "<?= base_url('Report/getsale')?>",
        data: {'to':to,'from':from },
        type: 'post',
        success: function(data){
            alert(data);
            $("#test").html(chart);
           //$("#test1").css('display','none');
            //$("#test").css("display", "block");
            //$("#tset").show();
            
          }
    });
});

</script>
<script type="text/javascript">
   var chart = Highcharts.chart('Barchart', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Sale in Barchart Last Week'
    },
   
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amount of sales'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Sale: <b>{point.y:.2f}</b>'
    },
    series: [{
        name: 'Population',
        data: [<?php foreach ($data as $key => $value) {echo "['$key',$value]"; echo ","; }?>],
           
       
    }]
});
</script>
<script type="text/javascript">
   Highcharts.chart('piechart', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Sale in Barchart Last Week'
    },
   
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amount of sales'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Sale: <b>{point.y:.2f}</b>'
    },
    series: [{
        name: 'Population',
        data: [<?php foreach ($data as $key => $value) {echo "['$key',$value]"; echo ","; }?>],
           
       
    }]
});
</script>

