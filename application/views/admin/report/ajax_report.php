<?php 

$begin = new DateTime( $to );
$end = new DateTime( $from );
$end = $end->modify( '+1 day' ); 

$interval = new DateInterval('P1D');
$daterange = new DatePeriod($begin, $interval ,$end);
 $data = [];
foreach($daterange as $date){
     $day = $date->format("Y-m-d");
  foreach ($payments as $key => $value) {
    $date = $value->orderDate;
    //if ( strtotime($day) == strtotime($date) ){
    if($day == $date){
      echo $day.'<br>';
      echo $value->amt.'<br>';
      $data [$day]= $value->amt;
   }  
  } 
 }
  //echo "<pre>";
 //  print_r($daterange);
  //print_r($payments);exit();
 // print_r($data);exit();
?>
 <div class="row">
  <div class ="col-md-6" style="border: 1px solid black;">
    <div id="linechart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
  <div class ="col-md-6" style="border: 1px solid black;">
    <div id="Barchart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
  </div>
</div>
<br>
  <div class="row">
    <div class="col-md-6 col-md-offset-3" style="border: 1px solid black;">
      <div id="piechart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
  </div>
  </br>
  </br>
<script type="text/javascript">
  Highcharts.chart('linechart', {
 chart: {
        type: 'line'
    },
  title: {
    text: 'Sale in linechart'
  },
  yAxis: {
    title: {
      text: 'Amount of sales'
    }
  },
   xAxis: {
        categories: [<?php foreach ($payments as $key => $value) {
          $days = date('d', strtotime($value->orderDate));echo $days; echo ","; }?>]
    },
  plotOptions: {
    series: {
      label: {
        connectorAllowed: false
      },
    }
  },

  series: [{
    name: 'Sale',
    data: [<?php foreach ($payments as $key => $value) {echo $value->amt; echo ","; }?>]
  }],

  

});


</script>
<script type="text/javascript">
  Highcharts.chart('Barchart', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Sale in Barchart'
    },
   
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amount of sales'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Sale: <b>{point.y:.2f}</b>'
    },
    series: [{
        name: 'Population',
        data: [<?php foreach ($payments as $key => $value) {echo "['$value->orderDate',$value->amt]"; echo ","; }?>],
           
       
    }]
});
</script>
<script type="text/javascript">
  Highcharts.chart('piechart', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Sale in Barchart'
    },
   
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Amount of sales'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Sale: <b>{point.y:.2f}</b>'
    },
    series: [{
        name: 'Population',
        data: [<?php foreach ($payments as $key => $value) {echo "['$value->orderDate',$value->amt]"; echo ","; }?>],
           
       
    }]
});
</script>