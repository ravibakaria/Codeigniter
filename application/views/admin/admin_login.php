<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
<div class="row">
  <div class ="col-md-4 col-md-offset-4" style="border: 1px solid black;margin-top:200px;">
	<h2>Admin Login</h2>
	  <form action="<?php echo base_url('Admin/login');?>" method = "post">
	    <div class="form-group">
	      <label for="email">Email:</label>
	      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
	      <?php echo form_error('email'); ?>
	    </div>
	    <div class="form-group">
	      <label for="pwd">Password:</label>
	      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
	      <?php echo form_error('pwd'); ?>
	    </div>
	    <button type="submit" class="btn btn-default">Submit</button>
		</form>
		<?php if($this->session->flashdata('message')){?>
	  	<div class="alert alert-warning">
			<strong>Error!</strong> <?php echo $this->session->flashdata('message');?>.
		</div>
		<?php } ?>
</div>
</div>
</div>

</body>
</html>
