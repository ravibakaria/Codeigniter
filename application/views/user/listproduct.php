
<?php include('user_nav.php');?>
<div class="container">
 <?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-success">
		<strong>Thank you!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>
 <?php if($this->session->flashdata('message_re')){?>
  	<div class="alert alert-warning">
		<strong>Error!</strong> <?php echo $this->session->flashdata('message_re');?>.
	</div>
	<?php } ?>
<div class="row">
<?php foreach ($products as $key => $value) {?>
  <a href="<?php echo base_url().'User/product_detail/'.$value->id;?>">
  <div class ="col-md-4" style="text-align: left; border: 1px solid black;">
	<div class="col-md-10">
	<?php $img = $this->db->get_where('images',['product_id'=>$value->id])->row();?>
		<img src="<?= base_url().'uploads/'.$img->img_name;?>" width = "350" height = "400px" >
	</div>
	<div class="row">
		<div class="col-md-6">
			NAME : <?= $value->name;?>
		</div>
		<div class="col-md-6" style="text-align: right;">
			Rs : <?= $value->pries;?> /-
		</div>
		<div class="col-md-12">
			<?= $value->Description;?>
		</div>
	</div>
</div></a>
<?php } ?>
</div>
</div>

</body>
</html>