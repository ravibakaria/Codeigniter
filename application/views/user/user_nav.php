<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?= base_url('User')?>">BigSale.com</a>
    </div>
    <ul class="nav navbar-nav">
      <!-- <li class=""><a href="<?= base_url('Admin/dashbord')?>">Home</a></li>
      <li><a href="<?= base_url('Admin/list_product')?>">Products </a></li> -->
      <!-- <li><a href="#">Page 2</a></li>
      <li><a href="#">Page 3</a></li> -->
    </ul>
     <ul class="nav navbar-nav navbar-right">
     <?php if(!empty($this->session->userdata('user'))){?>
     <li><a href="<?= base_url('User/show_order')?>"><span class="glyphicon glyphicon-gift"> </span> My Orders</a></li>
      <li><a href="<?= base_url('User/show_cart')?>" id="cart"><i class="fa fa-shopping-cart"></i> Cart <span class="badge"><?= $this->cart->total_items()?></span></a></li>
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata('user')[0]->name;?></a></li>
      <li><a href="<?= base_url('User/logout')?>" ><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
      <?php }else{ ?>
       <li><a href="<?= base_url('User/show_cart')?>" id="cart"><i class="fa fa-shopping-cart"></i> Cart <span class="badge"><?= $this->cart->total_items()?></span></a></li>
      <li><a href="<?= base_url('User/sign_up')?>"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="<?= base_url().'User/login'?>"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      <?php } ?>
    </ul>
  </div>
</nav>
