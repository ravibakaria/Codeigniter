<?php include('user_nav.php');?>

<div class="container">
<form action="<?= base_url().'User/update_cart'?>" method  = "post">

<table cellpadding="6" cellspacing="1" style="width:100%" border="0" class="table table-striped">
<tr>
        <th>QTY</th>
        <th>Item Description</th>
        <th style="text-align:right">Item Price</th>
        <th style="text-align:right">Sub-Total</th>
</tr>

<?php $i = 1; 
$invoice = rand(1,500);
// echo form_hidden($i.'[invoice_no1]', $invoice); 
?>
<input type = "hidden" id="INDUSTRY_TYPE_ID" tabindex="4" maxlength="12" size="12" name="INDUSTRY_TYPE_ID" autocomplete="off" value="Retail">
<input type = "hidden" id="CHANNEL_ID" tabindex="4" maxlength="12" size="12" name="CHANNEL_ID" autocomplete="off" value="WEB">
<input type = "text" id="CUST_ID" tabindex="2" maxlength="12" size="12" name="CUST_ID" autocomplete="off" value="<?php if($this->session->userdata('user')){ echo $this->session->userdata('user')[0]->id;}?>">
<input type="hidden" name="invoice_no"  value="<?= $invoice;?>">
<?php foreach ($this->cart->contents() as $items): ?>
        <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
        <?php echo form_hidden($i.'[price]', $items['price']); ?>
        <?php echo form_hidden($i.'[invoice_no]', $invoice); ?>
        <?php //echo form_hidden($i.'[subtotal]', $items['subtotal']); ?>
        <?php echo form_hidden($i.'[product_name]', $items['name']); ?>
        <?php echo form_hidden($i.'[total_amt]', $this->cart->total()); ?>
        <?php if($this->session->userdata('user')){    
                        echo form_hidden($i.'[u_id]', $this->session->userdata('user')[0]->id);

                } else {
                        echo form_hidden($i.'[u_id]', '0');
                        } ?>

        <tr>
                <td><?php echo form_input(array('name' => $i.'[qty]', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?></td>
                <td>
                        <?php echo $items['name']; ?>

                </td>
                <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
                <td style="text-align:right">$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
        </tr>

<?php $i++; ?>

<?php endforeach; ?>
<?php echo form_hidden('total_amt', $this->cart->total()); ?>
<tr>
        <td colspan="2"> </td>
        <td class="right"><strong>Total</strong></td>
        <td></td>
        <td class="right">$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
</tr>

</table>
<?php if($this->cart->format_number($this->cart->total()) !='0'){?>
<p><?php echo form_submit(['class'=>'btn btn-primary'], 'Update your Cart'); ?><input type="submit" name="order" class="btn btn-primary" value="COD"> <?php if($this->session->userdata('user')){?><input type="submit" name="payment" class="btn btn-primary" value="payment"></p>
<?php } } ?>
</div>
