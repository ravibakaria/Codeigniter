<!DOCTYPE html>
<html lang="en">
<head>
  <title>Sign up </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
 
  <div class="row">
  <div class ="col-md-4 col-md-offset-4">
   <h2>Sign up Form</h2>
  	<form action="<?= base_url('User/sign_up_action')?>" method="post">
    <div class="form-group">
      <label for="name">Name:</label>
      <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?php echo set_value('name'); ?>">
       <?php echo form_error('name'); ?>
    </div>
     <div class="form-group">
      <label for="email">Email:</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo set_value('email'); ?>">
       <?php echo form_error('email'); ?>
    </div>
    <div class="form-group">
      <label for="password">Password:</label>
      <input type="password" class="form-control" id="password" placeholder="Enter password" name="password" value="<?php echo set_value('password'); ?>">
       <?php echo form_error('password'); ?>
    </div>
    <div class="form-group">
      <label for="passconf">Confirmation Password:</label>
      <input type="password" class="form-control" id="passconf" placeholder="Enter password" name="passconf" value="<?php echo set_value('passconf'); ?>">
       <?php echo form_error('passconf'); ?>
    </div>
    
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
  </div>
  
</div>

</body>
</html>
