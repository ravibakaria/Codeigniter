import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Utils } from '@core/helper/utils';
import { AlertService } from '@core/services/common';
import { first } from 'rxjs/operators';
import { ResourceService, ResourceFilePath } from '@core/services/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { jqxDataTableComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatatable';
import { jqxTreeComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxtree';
import { Button } from 'selenium-webdriver';
import { DocumentControlService } from '@core/services/admin';
import { StorageService, StorageKey } from '@core/services/common';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
// import { type } from 'os';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'document-control-master',
  templateUrl: 'document-control-master.component.html'
})

export class DocumentControlMasterComponent implements OnInit, AfterViewInit {

  @ViewChild('myTree') myTree: jqxTreeComponent;
  @ViewChild('DocumentDetailsTable') DocumentDetailsTable: jqxDataTableComponent;
  @ViewChild('PaymentBankTable') PaymentBankTable: jqxGridComponent;
  @ViewChild('UserAuthorizationTable') UserAuthorizationTable: jqxDataTableComponent;
 // @ViewChild('PaymentBankTable') PaymentBankTable: jqxDataTableComponent;
  @ViewChild('addAmountButton') addAmountButton: any;
  @ViewChild('addUserButton') addUserButton: any;
  @ViewChild('addBankButton') addBankButton: any;

  doccontrolForm: FormGroup;
  rowIndex: number;
  treeMenus: any;
  documentDataAdapter: any;
  userDataAdapter: any;
  bankDataAdapter: any;
  documentColumn: any[];
  userAuthorizationColumn: any[];
  paymentBankColumn: any[];
  buttonDisabled = false;
  isValiDErrro = false;
  isEdit = false;
  isView = false;
  isAdd = true;
  treeId = 0;
  resourceModel: any = {};
  submitted = false;
  isDataView: Boolean;
  locationData: any;
  currentUser:any;
  companyData:any;
  financePeriod: any;
  mainData: any[]=[];
  treeMenuData: any[] = [];
  isAuthRequired:boolean;
  type:string;
  subType:string;
  isMultiAuthRequired:boolean;
  isLocationRequired:boolean;
  isAutoNoGenerateRequired:boolean;
  isDocLevelDetailChanged:boolean;
  isDocSerialDetailChanged:boolean;
  editUserdata:any;
  userauthlevel:any[] = [{ 'levels': '1'},
  {'levels': '2'},
  {'levels': '3'}, {'levels': '4'}, {'levels': '5'}];

  siteName:any;
  commonResourceModel: any = {};
  documentData: any[] = [];
  userAuthData: any[] = [];
  paymentBankData: any[] = [];

  hierarchicalRequired:any[]=[{
    'hierarchicalRequired': 'Y'
  },{'hierarchicalRequired': 'N'}];

  isDefault:any[]=[
                  {'isDefault': 'Y' },
                  {'isDefault': 'N',}
                ];

  // for label
  constructor(
    private alertService: AlertService,
    private resourceService: ResourceService,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private documentControlService: DocumentControlService,
    private storageService: StorageService
  ) {
    this.buttonDisabled = false;
  }

  get f() { return this.doccontrolForm.controls; }
  // doccontrolForm: FormGroup;
  private onbuildForm() {
    this.doccontrolForm = this.formBuilder.group({
      eouUnit: [this.isAuthRequired],
      multiautho: [this.isMultiAuthRequired],
      eouUnit2: [this.isAutoNoGenerateRequired],
      multiautho2: [this.isLocationRequired],
    });
  }

  private loadPageMode() {
    const currentUrl = this.router.url;
    if (currentUrl.includes('edit')) {
      this.isEdit = true;
      this.isView = false;
      this.isAdd = true;
    } else if (currentUrl.includes('view')) {
      this.isView = true;
      this.isEdit = false;
      this.isAdd = false;
    } else if (currentUrl.includes('add')) {
      this.isView = false;
      this.isEdit = true;
      this.isAdd = true;
    }
  }

  cancelClicked(){
    this.router.navigate(['/admin/masters/doccontrol/view/1']);
   }

  onEditClick(event: any): void {
    let editId = 0;
    this.activatedRoute.params.subscribe(parms => {
      editId = parms['id'];
    });
    this.router.navigate(['/admin/masters/doccontrol/edit/1']);
  }

  changeToView(event: any): void {
    let viewId = 0;
    this.activatedRoute.params.subscribe(parms => {
      viewId = parms['id'];
    });
  }

  saveChange(event: any): void {
    if(this.isValiDErrro == true){
      return;
    }
    const save = true;
    let documentLevelModel: { 
      'CompanyId': number, 'LocationId': number, 'DocumentType': string ,'DocumentSubType':string, 'Level':number,'HierarchicalRequired':string,'ValueLimit':number, 'UserId':number
    }[] = [];
    let documentDetailsGridSource = this.DocumentDetailsTable.source();
    if (documentDetailsGridSource['records'] && documentDetailsGridSource['records'].length > 0) {
      documentDetailsGridSource['records'].forEach(element => {
        documentLevelModel.push({ 
          'CompanyId': element.companyId, 'LocationId': element.locationId, 'DocumentType': element.documentType,'DocumentSubType': element.documentSubType,'Level': element.level,'HierarchicalRequired':element.hierarchicalRequired,'ValueLimit':element.valueLimit,'UserId':element.UserId
        });
      });
    }

    let documentUserLevel: { 
      'CompanyId': number, 'LocationId': number, 'DocumentType': string ,'DocumentSubType':string, 'Level':number,'SubLevel':number,'AuthUserId':number, 'UserCode':string
    }[] = [];
    let userAuthorizationGridSource = this.UserAuthorizationTable.source();
    if (userAuthorizationGridSource['records'] && userAuthorizationGridSource['records'].length > 0) {
      userAuthorizationGridSource['records'].forEach(element => {
        let selectedUserCode = this.editUserdata.find(x => x.userName == element.user);
        let userid ;
        let usercode;
        if(selectedUserCode != undefined){
          userid = selectedUserCode.userID;
          usercode = selectedUserCode.userCode;
        }else{
          userid = element.AuthUserId;
          usercode = element.userCode;
        }
        documentUserLevel.push({
          'CompanyId': element.companyId, 'LocationId': element.locationId, 'DocumentType': element.documentType,'DocumentSubType': element.documentSubType,'Level': element.levels,'SubLevel':element.subLevel,'AuthUserId':userid,'UserCode':usercode
        });
      });
    }

    let documentNoGeneration: { 
      'LastSrNO':number,'IsDefault':string,'OprationFlag':string,'GroupCodeDesc':string,'DocumentYear':string,'GroupCode':string,'DocumentType': string ,'DocumentSubType':string, 'LocationId': number,'LoggedInAsLocationId':number,'UserId':number
    }[] = [];
    let paymentBankTableGridSource = this.PaymentBankTable.source();

    if (paymentBankTableGridSource['records'] && paymentBankTableGridSource['records'].length > 0) {
      paymentBankTableGridSource['records'].forEach(element => {
        let locationId = this.siteName.find( x => x.locationName == element.site);
        let LoggedInAsLocationId;
        if(locationId != undefined){
          LoggedInAsLocationId = locationId.locationId;
        }else{
          LoggedInAsLocationId = element.LoggedInAsLocationId;
        }
        documentNoGeneration.push({ 
          'LastSrNO':element.LastSrNO,'IsDefault':element.isDefault,'OprationFlag':'','GroupCodeDesc':element.GroupCodeDesc,'DocumentYear':element.documentYear,'GroupCode':element.groupCode,'DocumentType': this.type,'DocumentSubType':this.subType, 'LocationId': this.locationData.id,'LoggedInAsLocationId':LoggedInAsLocationId,'UserId':element.UserId
        });
      });
    }
   
    const data = {
      "companyId": this.companyData.id,
      "locationId": this.locationData.id,
      "type": this.type,
      "subType": this.subType,
      "isAuthRequired": this.doccontrolForm.value.eouUnit,
      "isMultiAuthRequired": this.doccontrolForm.value.multiautho,
      "isAutoNoGenerateRequired": this.doccontrolForm.value.eouUnit2,
      "isLocationRequired": this.doccontrolForm.value.multiautho2,
      "createdBy": this.currentUser.id,
      "isDocLevelDetailChanged": true,
      "isDocSerialDetailChanged": true,
      "level": 1,
      "documentLevel":documentLevelModel,
      "documentUserLevel":documentUserLevel,
      "documentNoGeneration": documentNoGeneration,
    }
     this.create(data); 
  }

  create(data) {
    this.documentControlService.save(data).pipe(first()).subscribe(
     response => {
      this.alertService.showSuccess(Utils.formatString(this.commonResourceModel[response.message], this.resourceModel.admn_admmaster_documentcontrolmaster_save));
      this.router.navigate(['/admin/masters/doccontrol/view/1']);
     },
     error => {
      let msg: string = '';
      if(error.error.message.indexOf(' ')>-1)
      {
        msg = error.error.message;
      }
      else{
        msg = this.resourceModel[error.error.message] || this.commonResourceModel[error.error.message] || this.commonResourceModel['UnhandledErrorKey'];
      }
      this.alertService.showError(Utils.formatString(msg, this.resourceModel.admn_admmaster_documentcontrolmaster_save));   
     });
 }

//  checkForErrors(form,errorMsg) {
//   let newErr = {};
//   if (errorMsg) {
//     errorMsg.forEach(element => {
//       newErr[element.errorCode] = true;
//       if (form.controls[element.propertyName]) {
//         form.controls[element.propertyName].setErrors(newErr);
//       }
//     });
//   }
// }
// private Groupcode(){
//     this.documentControlService.Groupcode('B',34,'V').subscribe(res=>{
//       //console.log(res);
//     },
//     err =>{
//       console.log(err);
//     })
//   }
  private bindDocumentGrid() {
    const source: any = {
      dataFields: [
        { name: 'id', type: 'Number' },
        { name: 'levels', type: 'string' },
        { name: 'hierarchicalRequired', type: 'string' },
        { name: 'limit', type: 'Number' },
      ],
      datatype: 'json',
      id: 'id',
      localdata: this.documentData
    };
    this.documentDataAdapter = new jqx.dataAdapter(source);
  }

  private bindDocumentLevelDetails() {
    this.documentColumn = [
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_lvls1, datafield: 'level', width: 200,
        validation: (cell: any, value: string): any => {
          if (cell.value === '' || cell.value === null) {
            this.isValiDErrro = true;
            return {
              message: 'Level details is required', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_hrreq, datafield: 'hierarchicalRequired',
        columntype: 'template', width: 200,
        createEditor: (row: any, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
          // construct the editor.
          editor.jqxDropDownList({
            source: this.hierarchicalRequired, displayMember: 'hierarchicalRequired',
            valueMember: 'hierarchicalRequired', width: width, height: height, placeHolder: 'Select Item', selectedIndex: 0
          });
        },
        initEditor: (row: any, cellvalue: any, editor: any, celltext: any, width: any, height: any): void => {
          // set the editor's current value. The callback is called each time the editor is displayed.
          if (!cellvalue) {
            cellvalue = '';
          }
          const inputField = editor.find('input');
          inputField.val(cellvalue);
        },
        getEditorValue: (row: any, cellvalue: any, editor: any): void => {
          // return the editor's value.
          return editor.find('input').val();
        },
        validation: (cell: any, value: string): any => {
          if (value === '' || value == null) {
            this.isValiDErrro = true;
            return {
              message: 'Hierarchical is required', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_vallimt, datafield: 'valueLimit',
        
        // cellbeginedit:(row) => {
        //  debugger;
        //   // let rowIndex = this.DocumentDetailsTable.getselectedrowindex();
        //   // let rowId = this.grid[index].getrowid(rowIndex);
          
        //   // let rowData = this.grid[index].getrowdatabyid(rowId);
        //   // if(rowData !== undefined && rowData.isExisting !== false) {
        //   //   return false;
        //   // }
        // },
        validation: (cell: any, value: number): any => {
          debugger
          //let value1 = this.DocumentDetailsTable.getRows();
          let value1 = this.DocumentDetailsTable.getCellValue(cell.row,'level');
          let limitvalue  = value1 * 50000;
          if(value > limitvalue){
            this.isValiDErrro = true;
            return {
              message: `limit value not greaterthen  ${limitvalue}`, result: false
            };
          }
         
          if ((value === NaN || value === null)) {
            this.isValiDErrro = true;
            return {
              message: 'limit is required', result: false
            };
          }
          if( value.toString().length > 9){
            this.isValiDErrro = true;
            return {
              message: 'limit is not greater than 9 digit', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        },
      }
    ];
  }

  private bindUserGrid() {
    const source: any = {
      dataFields: [
        { name: 'id', type: 'Number' },
        { name: 'levels', type: 'string' },
        { name: 'subLevel', type: 'Number' },
        { name: 'user', type: 'string' },
      ],
      datatype: 'json',
      id: 'id',
      localdata: this.userAuthData
    };
    this.userDataAdapter = new jqx.dataAdapter(source);
  }

  private bindDocumentUserLevelDetails() {
    let self = this;
    this.userAuthorizationColumn = [
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_lvls2, datafield: 'levels', columntype: 'template', width: 200,
        createEditor: (row: any, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
          // construct the editor.
          //debugger;
          console.log(this.getEditorDataAdapter);
          editor.jqxDropDownList({
            source: this.getEditorDataAdapter('level', 'user'), displayMember: 'level', placeHolder: 'Select Item',
            valueMember: 'level', width: width, height: height, selectedIndex: 0
          });
        },
        initEditor: (row: any, cellvalue: any, editor: any, celltext: any, width: any, height: any): void => {
          // set the editor's current value. The callback is called each time the editor is displayed.
          if (!cellvalue) {
            cellvalue = '';
          }
          const inputField = editor.find('input');
          inputField.val(cellvalue);
        },
        getEditorValue: (row: any, cellvalue: any, editor: any): void => {
          // return the editor's value.
          return editor.find('input').val();

        },
        validation: (cell: any, value: string): any => {
          if (value === '' || value == null) {
            this.isValiDErrro = true;
            return {
              message: 'Levels is required', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_sblvl, datafield: 'subLevel', width: 200,
         validation: (cell: any, value: Number): any => {
      

        //  //  let value2 = this.DocumentDetailsTable.selectRow(1);
        //  let value1 = this.UserAuthorizationTable.getRows();
        //  let value2 = this.UserAuthorizationTable.getCellValue(0,'levels');
        //    debugger;
        // //   let value1 = this.DocumentDetailsTable.getCellValue(cell.index,'levels');
        // //   let selectedUserCode = value2.find(x => x.value2 == value2.hierarchicalRequired);
        // //  debugger;
          if (value === NaN || value == null) {
            this.isValiDErrro = true;
            return {
              message: 'sub Level details is required', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_usr, datafield: 'user', columntype: 'template',
        createEditor: (row: any, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
          // construct the editor.
          editor.jqxDropDownList({
            source: this.editUserdata, displayMember: 'userName', placeHolder: 'Select Item',
            valueMember: 'userName', width: width, height: height, selectedIndex: 0
          });
        },
        initEditor: (row: any, cellvalue: any, editor: any, celltext: any, width: any, height: any): void => {
          // set the editor's current value. The callback is called each time the editor is displayed.
          if (!cellvalue) {
            cellvalue = '';
          }
          const inputField = editor.find('input');
          inputField.val(cellvalue);
        },
        getEditorValue: (row: any, cellvalue: any, editor: any): void => {
          // return the editor's value.
          return editor.find('input').val();
        },
        validation: (cell: any, value: string): any => {
          if (value === '' || value == null) {
            this.isValiDErrro = true;
            return {
              message: 'User is required', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        },
      },
     { text: this.resourceModel.admn_admmaster_documentcontrolmaster_sblvl, datafield: 'UserCode', width: 200,hidden:true},
     { text: this.resourceModel.admn_admmaster_documentcontrolmaster_sblvl, datafield: 'AuthUserId', width: 200,hidden:true}
    ];
  }

  private bindBankGrid() {
    const source: any = {
      dataFields: [
        { name: 'id', type: 'Number'},
        { name: 'documentYear', type: 'string' },
        { name: 'groupCode', type: 'string' },
        { name: 'GroupCodeDesc', type: 'string' },
        { name: 'site', type: 'string' },
        { name: 'isDefault', type: 'string' },
        { name: 'LastSrNO ', type: 'Number' },
      ],
      datatype: 'json',
      id: 'id',
      localdata: this.paymentBankData
    };

    this.bankDataAdapter = new jqx.dataAdapter(source);

  }

  getEditorDataAdapter = (datafield: any, type: any): any => {
    let source: any;
    switch (type) {
      case 'user':
        let documentLevelModel = [];
          let documentDetailsGridSource = this.DocumentDetailsTable.source();
          if (documentDetailsGridSource['records'] && documentDetailsGridSource['records'].length > 0) {
            documentDetailsGridSource['records'].forEach(element => {
              documentLevelModel.push({ 
                'CompanyId': element.companyId, 'LocationId': element.locationId, 'DocumentType': element.documentType,'DocumentSubType': element.documentSubType,'level': element.level,'hierarchicalRequired':element.hierarchicalRequired,'limit':element.valueLimit,'UserId':element.UserId
              });
            });
          }
        source = {
          dataFields: [
            { name: 'id', type: 'Number' },
            { name: 'level', type: 'string' },
            { name: 'hierarchicalRequired', type: 'string' },
            { name: 'limit', type: 'Number' },
          ],
          datatype: 'json',
          id: 'id',
          localdata: documentLevelModel
         // this.documentData
        };
        // console.log(documentLevelModel);
        documentLevelModel = [];
        break;
      case 'user1':
        source = {
          dataFields: [
            { name: 'id', type: 'Number' },
            { name: 'levels', type: 'string' },
            { name: 'subLevel', type: 'Number' },
            { name: 'user', type: 'string' },
          ],
          datatype: 'json',
          id: 'id',
          localdata: this.documentData
        };
        break;
      case 'bank':
        source = {
          dataFields: [
            { name: 'id', type: 'Number' },
            { name: 'documentYear', type: 'string' },
            { name: 'groupCode', type: 'string' },
            { name: 'description', type: 'string' },
            { name: 'site', type: 'string' },
            { name: 'isDefault', type: 'string' },
            { name: 'LastSrNO', type: 'Number' },
          ],
          datatype: 'json',
          id: 'id',
          localdata: this.paymentBankData
        };
        break;
    }
    const dataAdapter = new jqx.dataAdapter(source, { uniqueDataFields: [datafield] });
    return dataAdapter;
  }

  private bindDocumentNumberGenerationDetails() {
    this.paymentBankColumn = [
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_docyr, datafield: 'documentYear', width: 200, columntype:'template', 
        cellbeginedit:(row) => {
            return false;
        },
        validation: (cell: any, value: string): any => {
         // debugger;
         
          if (value === '' || value == null ) {
            this.isValiDErrro = true;
            return {
              message: 'document year is required', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_grpcode, datafield: 'groupCode', width: 200,
        validation: (cell: any, value: string): any => {  
          // let value1 = this.PaymentBankTable.getrowdata(cell.row);
          // let paymentBankTableGridSource = this.PaymentBankTable.source();
          // let count = [];
          // if (paymentBankTableGridSource['records'] && paymentBankTableGridSource['records'].length > 1) {
          //   paymentBankTableGridSource['records'].forEach(element => {
          //     if(element.site == value1.site && element.groupCode == value && element.documentYear == value1.documentYear){
          //         count.push(value1);
          //     }
          //   });
          // }
          if (value === '' || value == null ) {
            this.isValiDErrro = true;
            return {
              message: 'group code is required', result: false
            };
          }
          // if(count.length >= 1 ){
          //   this.isValiDErrro = true;
          //   count = [];
          //   return {
          //     message: 'group code cant be duplicate', result: false
          //   };
          // }
          this.isValiDErrro = false;
         // count = [];
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_grpcodedesc, datafield: 'GroupCodeDesc', width: 300,
        validation: (cell: any, value: string): any => {
          if (value === '' || value == null) {
            this.isValiDErrro = true;
            return {
              message: 'group code description is required', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_site, datafield: 'site', columntype: 'template', width: 300,
        createEditor: (row: any, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
          // construct the editor.
          editor.jqxDropDownList({
            source: this.siteName, displayMember: 'locationName', placeHolder: 'Select Item',
            valueMember: 'locationName', width: width, height: height, selectedIndex: 0
          });
        },
        initEditor: (row: any, cellvalue: any, editor: any, celltext: any, width: any, height: any): void => {
          // set the editor's current value. The callback is called each time the editor is displayed.
          if (!cellvalue) {
            cellvalue = '';
          }
          const inputField = editor.find('input');
          inputField.val(cellvalue);
        },
        getEditorValue: (row: any, cellvalue: any, editor: any): void => {
          // return the editor's value.
          return editor.find('input').val();
        },
        validation: (cell: any, value: string): any => {
         
          if (value === '' || value == null) {
            this.isValiDErrro = true;
            return {
              message: 'Site details is required', result: false
            };
          }
          this.isValiDErrro = false;
         
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_isdef, datafield: 'isDefault', columntype: 'template', width: 200,
        createEditor: (row: any, cellvalue: any, editor: any, cellText: any, width: any, height: any): void => {
          // construct the editor.
          editor.jqxDropDownList({
            source: this.isDefault, displayMember: 'isDefault', placeHolder: 'Select Item',
            valueMember: 'isDefault', width: width, height: height, selectedIndex: 0
          });
        },
        initEditor: (row: any, cellvalue: any, editor: any, celltext: any, width: any, height: any): void => {
          // set the editor's current value. The callback is called each time the editor is displayed.
          if (!cellvalue) {
            cellvalue = '';
          }
          const inputField = editor.find('input');
          inputField.val(cellvalue);
        },
        getEditorValue: (row: any, cellvalue: any, editor: any): void => {
          // return the editor's value.
          return editor.find('input').val();
        },
        validation: (cell: any, value: string): any => {
          //debugger;
          let value1 = this.PaymentBankTable.getrowdata(cell.row);
          let paymentBankTableGridSource = this.PaymentBankTable.source();
          let count = [];
          if (paymentBankTableGridSource['records'] && paymentBankTableGridSource['records'].length > 1) {
            paymentBankTableGridSource['records'].forEach(element => {
              if(value == 'Y'){

              
              if(element.site == value1.site && element.groupCode == value1.groupCode && element.documentYear == value1.documentYear){
                  count.push(value1);
              }
            }
            });
          }
          if (value === '' || value == null) {
            this.isValiDErrro = true;
            return {
              message: 'Is Default is required', result: false
            };
          }
          if(count.length >= 1 ){
            this.isValiDErrro = true;
            count = [];
            return {
              message: 'Is Default be duplicate this entry', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        }
      },
      {
        text: this.resourceModel.admn_admmaster_documentcontrolmaster_lstsrno, datafield: 'LastSrNO',
     
        validation: (cell: any, value: any): any => {
          debugger;
          if (value === NaN || value == null || value =="") {
            this.isValiDErrro = true;
            return {
              message: 'Last sr. no. is required', result: false
            };
          }
          this.isValiDErrro = false;
          return true;
        }
      }
    ];
  }
 
  loadResoruce(language) {
    this.resourceService.loadResoruce(language).pipe(first()).subscribe(
      response => {
        this.resourceModel = response;
        this.bindDocumentLevelDetails();
        this.bindDocumentUserLevelDetails();
        this.bindDocumentNumberGenerationDetails();
      }, error => {
        this.alertService.showError(Utils.formatError(error));
      }
    );

    this.resourceService.loadResoruce(ResourceFilePath.common).pipe(first()).subscribe(
      response => {
        this.commonResourceModel = response;
      },
      error => {
        this.alertService.showError(Utils.formatError(error));
      });
  }

  private onLoad() {
    this.locationData = JSON.parse(this.storageService.getValue(StorageKey.locationData));
    this.currentUser = JSON.parse(this.storageService.getValue(StorageKey.currentUser));
    this.companyData = JSON.parse(this.storageService.getValue(StorageKey.companyData));
    this.financePeriod = JSON.parse(this.storageService.getValue(StorageKey.financePeriod));
    this.bindTree();
    this.getUserList();
    this.getLocationList();
    this.loadResoruce(ResourceFilePath.doccontrol);
    this.onbuildForm();
    this.bindDocumentGrid();
    this.bindUserGrid();
    this.bindBankGrid();
    console.log(this.financePeriod[0].financialYear);
    //console.log(this.PaymentBankTable);
  }

  ngOnInit() {
    this.loadPageMode();
    this.onLoad();
  }

  ngAfterViewInit() {
    // this.myTree.expandAll();
  }

  getWidth(): any {
    if (document.body.offsetWidth < 850) {
      return '100%';
    }
    return '100%';
  }
  chkAuthRequiredEnabled = true;
  chkMulAuthReqEnabled = true;
  chkAutoNoGnrtEnabled = true;
  chkAutoNoGnrtChecked= true;
  dgvDocSrNoReadOnly = true;

  onMenuClick(event: any): void { 
   
    const args = event.args;
    
    const item = this.myTree.getItem(args.element);
    
    this.treeId = item['id'];
    let list = this.treeMenuData.find(x=> x.id == this.treeId);
  
    var LocationId = this.locationData.id;
    

    // if (this.type == "IQ" || this.type == "TD" || this.type == "CD"){
    //   this.chkAuthRequiredEnabled = false;
    //      this.chkMulAuthReqEnabled = false;
    // }
    // if(this.type == "TD")
    //     {
    //       this.chkAutoNoGnrtEnabled = false;
    //       this.chkAutoNoGnrtChecked = true;
    //     }
    // if (this.type == "A1" || this.type == "A3")
    //     {
    //       this.chkAuthRequiredEnabled = false;
    //       this.chkMulAuthReqEnabled = false;
    //     }
    // if (this.type == "PJ" )
    //     {
    //       this.chkAuthRequiredEnabled = false;
    //       this.chkMulAuthReqEnabled = false;
    //       this.chkAutoNoGnrtEnabled = false;
    //     // this.chkAutoNoGnrtChecked = true;
    //     // this.dgvDocSrNoReadOnly = true;
    //     }
    if (parseInt(item['id'], 10) !== 1) {
      //this.isDataView = true;

    }
    if(list.subType != undefined){
      this.type= list.type;
      this.subType = list.subType;
      this.isDataView = true;
      this.getDocumentDetails(LocationId,this.type,this.subType);
    }
    
  
   
    
  }
  
  showDataTables(){
    const source1: any = {
      dataFields: [
        { name: 'id', type: 'Number' },
        { name: 'level', type: 'string' },
        { name: 'hierarchicalRequired', type: 'string' },
        { name: 'valueLimit', type: 'Number' },
        { name: 'companyId', type: 'Number' },
        { name: 'locationId', type: 'Number' },
        { name: 'documentType', type: 'string' },
        { name: 'documentSubType', type: 'string' },
        { name: 'documentSubType', type: 'string' },
        { name: 'UserId', type: 'number' },
      ],
      datatype: 'json',
      id: 'id',
      localdata: this.documentData
    };
    const source2: any = {
      dataFields: [
        { name: 'id', type: 'Number' },
        { name: 'levels', type: 'string' },
        { name: 'subLevel', type: 'Number' },
        { name: 'user', type: 'string' },
        { name: 'companyId', type: 'Number' },
        { name: 'locationId', type: 'Number' },
        { name: 'documentType', type: 'string' },
        { name: 'documentSubType', type: 'string' },
        { name: 'documentSubType', type: 'string' },
        { name: 'AuthUserId', type: 'number' },
        { name: 'UserCode', type: 'string' },
      ],
      datatype: 'json',
      id: 'id',
      localdata: this.userAuthData
    };
    const source3: any = {
      dataFields: [
        { name: 'id', type: 'Number' },
        { name: 'documentYear', type: 'string' },
        { name: 'groupCode', type: 'string' },
        { name: 'GroupCodeDesc', type: 'string' },
        { name: 'site', type: 'string' },
        { name: 'isDefault', type: 'string' },
        { name: 'LastSrNO', type: 'Number' },
        { name: 'companyId', type: 'Number' },
        { name: 'locationId', type: 'Number' },
        { name: 'documentType', type: 'string' },
        { name: 'documentSubType', type: 'string' },
        { name: 'documentSubType', type: 'string' },
        { name: 'UserId', type: 'number' },
        { name: 'OprationFlag', type: 'string' },
        { name: 'LoggedInAsLocationId', type: 'number' },
      ],
      cache: false,
      datatype: 'json',
      id: 'id',
      localdata: this.paymentBankData
    };
    this.documentDataAdapter = new jqx.dataAdapter(source1);
    this.userDataAdapter = new jqx.dataAdapter(source2);
   // this.bankDataAdapter = new jqx.dataAdapter(source3);
    //this.bankDataAdapter = new jqx.dataAdapter(source3);
    this.bankDataAdapter = new jqx.dataAdapter(source3);
    this.PaymentBankTable.source(this.bankDataAdapter);

    console.log(this.PaymentBankTable);
    console.log(this.UserAuthorizationTable);
    
}
private getUserList(){
  let data = {
    'pageSize': 1000,
    'sortField':'userName',
    'sortDirection':'ASC'
    //'pageNumber': 1,

  }
  this.documentControlService.getUserList(data).subscribe(res=>{
   // console.log(res);
    this.editUserdata = res.data
  },
  err =>{
    console.log(err);
  })
}
private getLocationList(){
  this.documentControlService.getLocationList(this.locationData.id,'S').subscribe(res=>{
    //console.log(res);
    this.siteName = res.data
  },
  err =>{
    console.log(err);
  })
}

private getDocumentDetails( location:number,type:string,subtype:string){
  this.documentControlService.getDocumentDetail(location,type,subtype).pipe(first()).subscribe(res => {
    // if(res.data.details == ''){
    //   this.isDataView = false;
    //   return;
    // }
    this.mainData = res.data;
    res.data.details.forEach(element => {
      this.isAuthRequired= element.isAuthRequired;
      this.type= element.type;
      this.subType= element.subType;
      this.isMultiAuthRequired= element.isMultiAuthRequired;
      this.isLocationRequired=element.isLocationRequired;
      this.isAutoNoGenerateRequired= element.isAutoNoGenerateRequired;
      this.isDocLevelDetailChanged= false;
      this.isDocSerialDetailChanged= true;
    });
   
    this.documentData =[];
    this.paymentBankData = [];
    this.userAuthData = [];
        let i = 0;
        res.data.documentLevels.forEach(element => {
          this.documentData.push({
            'id': i,
            'level': element.level,
            'hierarchicalRequired': element.hierarchicalRequired,
            'valueLimit': element.valueLimit,
           'companyId':this.companyData.id,
            'locationId':this.locationData.id,
            'documentType':this.type,
            'documentSubType':this.subType,
            'UserId':this.currentUser.id,
          
          });
          i++;
        });
        
        res.data.documentNoGenerations.forEach(element => {
          this.paymentBankData.push({
                'id': i,
                'documentYear': element.documentYear,
                'groupCode': element.groupCode,
                'GroupCodeDesc': element.groupCodeDesc,
                'site': element.locationName,
                'isDefault': element.isDefault,
                'LastSrNO': element.lastSrNO,
                'companyId':this.companyData.id,
                'locationId':this.locationData.id,
                'LoggedInAsLocationId': this.locationData.id,
                'documentType':this.type,
                'documentSubType':this.subType,
                'UserId':this.currentUser.id,
                'OprationFlag':'yes',
          });
          i++;
        });
      
        res.data.documentUserLevels.forEach(element => {
          this.userAuthData.push({
            'id': i,
            'levels': element.level,
            'subLevel': element.subLevel,
            'user': element.userFullName,
            'companyId':this.companyData.id,
            'locationId':this.locationData.id,
            'documentType':this.type,
            'documentSubType':this.subType,
            'AuthUserId':element.authUserId,
            'UserCode': element.userId
          });
          i++;
        });
        this.onbuildForm();
        this.showDataTables();
        
   },
   err =>{});
}
  private bindTree(){
    this.activatedRoute.params.subscribe(params => {
      this.documentControlService.getDocumentCodes().pipe(first()).subscribe(res => {
       //console.log(res)
       
        let parents = [];
        let i = 0;
        let j = 0;
        res.data.types.forEach(element => {
          this.treeMenuData.push({
            'id': i,
            'form': element.id,
            'parentid': null,
            'type': element.type,
            'text': element.type + ' - ' + element.description
          });
          parents.push(element.type);
          i++;
        })

        res.data.typeDetails.forEach(element => {
          this.treeMenuData.push({
            'id': i,
            'form': element.id,
            'parentid': parents.indexOf(element.type),
            'type': element.type,
            'subType': element.subType,
            'text': element.type + ' - ' + element.description
          });
        
          i++;
        })
        this.bindTreeMenu(this.treeMenuData);
      },
      err =>{});
    }); 
  }

  private bindTreeMenu(treeMenuData:any[]) {
    // prepare the data
    const data: any[] = treeMenuData;
    const treeSource = {
      datatype: 'json',
      datafields: [
        { name: 'id' },
        { name: 'parentid' },
        { name: 'type' },
        { name: 'subType' },
        { name: 'text' }
      ],
      id: 'id',
      localdata: data
    };
    // create data adapter & perform Data Binding.
    const dataAdapter = new jqx.dataAdapter(treeSource, { autoBind: true });
    this.treeMenus = dataAdapter.getRecordsHierarchy('id', 'parentid', 'items', [{ name: 'text', map: 'label' }]);
  }

  onClick(event: any): void {
  
    this.isView = false;
    switch (event.target.id) {
      case 'addAmountButton':
          let document = [];
          let documentDetailsGridSource = this.DocumentDetailsTable.source();
          if (documentDetailsGridSource['records'] && documentDetailsGridSource['records'].length > 0) {
            documentDetailsGridSource['records'].forEach(element => {
              document.push({ 
                'CompanyId': element.companyId, 'LocationId': element.locationId, 'DocumentType': element.documentType,'DocumentSubType': element.documentSubType,'level': element.level,'hierarchicalRequired':element.hierarchicalRequired,'limit':element.valueLimit,'UserId':element.UserId
              });
            });
          }
          let leval :any = '';
          if(document.length > 0){
              leval = document.length + 1;
          } else {
            leval = '1';
          }
        //  add new empty row.
        this.DocumentDetailsTable.addRow(null, {'level':leval,'companyId':this.companyData.id,
        'locationId':this.locationData.id,
        'documentType':this.type,
        'documentSubType':this.subType,
        'UserId':this.currentUser.id}, 'first');
        //  select the first row and clear the selection.
        this.DocumentDetailsTable.clearSelection();
        this.DocumentDetailsTable.selectRow(0);
        this.isEdit = true;
        //  edit the new row.
        this.DocumentDetailsTable.beginRowEdit(0);
        // this.updateButtons('add');
        break;
      case 'addUserButton':
        //  add new empty row.
        this.UserAuthorizationTable.addRow(null, {'companyId':this.companyData.id,
        'locationId':this.locationData.id,
        'LoggedInAsLocationId': this.locationData.id,
        'documentType':this.type,
        'documentSubType':this.subType
        }, 'first');
        //  select the first row and clear the selection.
        this.UserAuthorizationTable.clearSelection();
        this.UserAuthorizationTable.selectRow(0);
        this.isEdit = true;
        //  edit the new row.
        this.UserAuthorizationTable.beginRowEdit(0);
        // this.updateButtons('add');
        break;
      case 'addBankButton':
        //  add new empty row.
        // this.PaymentBankTable.addrow(1,{firstname: 'New Name'});
        this.PaymentBankTable.addrow(null,{'companyId':this.companyData.id,
        'locationId':this.locationData.id,
        'documentType':this.type,
        'documentSubType':this.subType,
        'UserId':this.currentUser.id,'OprationFlag':'', 
      'documentYear':this.financePeriod[0].financialYear}, 'first');

      this.PaymentBankTable.clearselection();
       this.PaymentBankTable.selectrow(0);
      // //edit the new row.
      
      //this.PaymentBankTable.endrowedit(0,true);
        // select the first row and clear the selection.
        // this.PaymentBankTable.clearSelection();
        // this.PaymentBankTable.selectRow(0);
        this.PaymentBankTable.beginrowedit(0);
        this.PaymentBankTable.focus();
       // this.PaymentBankTable.endrowedit(0,true);
     
        break;
      }
  }
  
  updateButtons(action: string): void {
    switch (action) {
      case 'Select':
        this.buttonDisabled = false;
        break;
      case 'Unselect':
        this.buttonDisabled = false;
        break;
      case 'Edit':
        this.buttonDisabled = true;
        break;
      case 'End Edit':
        this.buttonDisabled = false;
        break;
      case 'add':
        this.buttonDisabled = false;
        break;
    }
  }
  onRowSelect(event: any): void {
    this.rowIndex = event.args.index;
    console.log('row index',this.rowIndex);
    this.updateButtons('Select');
  }
  onRowUnselect(event: any): void {
    this.updateButtons('Unselect' );
  }
  onRowEndEdit(event: any){
    this.updateButtons('End Edit');
  }

  onRowEndEdit1(event: any){
    debugger;
    let value1 = this.PaymentBankTable.getrowdata(event.args.row);
    let paymentBankTableGridSource = this.PaymentBankTable.source();
    let count = [];
    if (paymentBankTableGridSource['records'] && paymentBankTableGridSource['records'].length > 1) {
      paymentBankTableGridSource['records'].forEach(element => {
        
      });
    }
    this.updateButtons('End Edit');
  }


  onRowBeginEdit(event: any): void {
    this.updateButtons('Edit');
  }
  onSublit() {
  }



  cellValueChanged2(event:any){
    console.log('cell value chages 2',event); 
  }

}
