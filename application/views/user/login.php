<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body style="background-color:  ">

<div class="container">
<?php if($this->session->flashdata('message_re')){?>
  	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $this->session->flashdata('message_re');?>.
	</div>
	<?php } ?>
<div class="row">
  <div class ="col-md-4 col-md-offset-4" style="border: 1px solid black;margin-top:200px;">
	<h2>User Login</h2>
	  <form action="<?php echo base_url('User/login_action');?>" method = "post">
	    <div class="form-group">
	      <label for="email">Email:</label>
	      <input type="email" class="form-control" id="email" placeholder="Enter email" value="<?php echo set_value('email');?>" name="email">
	      <?php echo form_error('email'); ?>
	    </div>
	    <div class="form-group">
	      <label for="pwd">Password:</label>
	      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd" value="<?php echo set_value('pwd');?>">
	      <?php echo form_error('pwd'); ?>
	    </div>
	    <button type="submit" class="btn btn-default">Submit</button>
		</form>
		
		<div id="main">
		<div id="login">
		<a href="<?=$google_login_url?>"class="waves-effect waves-light btn red"><i class="fa fa-google left"></i>Google login</a>
		<a href="<?= $login_url;?>"><i class=""></i>Facebook login</a>
		
		</div>
		</div>
	<?php if($this->session->flashdata('message')){?>
  	<div class="alert alert-warning">
		<strong>Error!</strong> <?php echo $this->session->flashdata('message');?>.
	</div>
	<?php } ?>
</div>
</div>
</div>

</body>
</html>
<!-- <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : 161762160591888,
      cookie     : true,
      xfbml      : true,
      version    : 'v3.3'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script> -->
