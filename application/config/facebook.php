<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['facebook_app_id']              = '284577232227322';
$config['facebook_app_secret']          = '3e00c994fd7c05065febef8c4943975a';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'User.php/flogin';
$config['facebook_logout_redirect_url'] = 'User.php/logout';
$config['facebook_permissions']         = array('public_profile', 'publish_actions', 'email');
$config['facebook_graph_version']       = 'v2.6';
$config['facebook_auth_on_load']        = TRUE;